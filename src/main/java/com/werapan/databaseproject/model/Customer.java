/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import com.werapan.databaseproject.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class Customer {
    private int id;
   private String tel;
    private String name;

    public Customer(int id,String name, String tel) {
        this.id = id;
        this.tel = tel;
        this.name = name;
        
    }
    public Customer(String name, String tel) {
        this.id = -1;
        this.tel = tel;
        this.name = name;
        
    }


    public Customer() {
        this.id = -1;
        this.tel = "";
        this.name="";
       
    }

    public int getId() {
        return id;
    }

    public String getTel() {
        return tel;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", tel=" + tel + ", name=" + name + '}';
    }

    

    
    public static Customer fromRS(ResultSet rs) {
        Customer user = new Customer();
        try {
            user.setId(rs.getInt("customer_id"));
            user.setName(rs.getString("customer_name"));
            user.setName(rs.getString("customer_tel"));
        } catch (SQLException ex) {
            Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return user;
    }

    
}
