/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import com.werapan.databaseproject.dao.CustomerDao;
import com.werapan.databaseproject.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Date;

/**
 *
 * @author werapan
 */
public class Reciept {

    private int id;
    private Date createdDate;
    private float total;
    private float crash;
    private int totalQty;
    private int userId;
    private int customerId;
    private User user;
    private Customer customer;
    private ArrayList<RecieptDetail> recieptDetail = new ArrayList();

    public Reciept(int id, Date createdDate, float total, float crash, int totalQty, int userId, int customerId) {
        this.id = id;
        this.createdDate = createdDate;
        this.total = total;
        this.crash = crash;
        this.totalQty = totalQty;
        this.userId = userId;
        this.customerId = customerId;
    }

    public Reciept(Date createdDate, float total, float crash, int totalQty, int userId, int customerId) {
        this.id = -1;
        this.createdDate = createdDate;
        this.total = total;
        this.crash = crash;
        this.totalQty = totalQty;
        this.userId = userId;
        this.customerId = customerId;
    }

    public Reciept(float total, float crash, int totalQty, int userId, int customerId) {
        this.id = -1;
        this.createdDate = null;
        this.total = total;
        this.crash = crash;
        this.totalQty = totalQty;
        this.userId = userId;
        this.customerId = customerId;
    }

    public Reciept() {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.crash = 0;
        this.totalQty = 0;
        this.userId = 0;
        this.customerId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getCrash() {
        return crash;
    }

    public void setCrash(float crash) {
        this.crash = crash;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
        this.userId = user.getId();
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
        this.customerId = customer.getId();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "Reciept{" + "id=" + id + ", createdDate=" + createdDate + ", total=" + total + ", crash=" + crash + ", totalQty=" + totalQty + ", userId=" + userId + ", customerId=" + customerId + ", user=" + user + ", customer=" + customer + '}';
    }

    public void addRecieptDetail(Product product, int qty) {
        RecieptDetail rd = new RecieptDetail(product.getId(),
                product.getName(), product.getPrice(), qty,qty+ product.getPrice(), -1);
        recieptDetail.add(rd);
        calculateTotal();
    }

    public void delRecieptDetail(RecieptDetail recieptDetail) {
        recieptDetail.remove(recieptDetail);
        calculateTotal();
    }

    public void calculateTotal() {
        int qty_total = 0;
        float total = 0.0f;
        for (RecieptDetail rd : recieptDetail) {
            total += rd.getTotalPrice();
            totalQty += rd.getQty();
        }

        this.totalQty = totalQty;
        this.total = total;
    }

    public static Reciept fromRS(ResultSet rs) {
        Reciept reciept = new Reciept();
        try {
            reciept.setId(rs.getInt("reciept_id"));
            reciept.setCreatedDate(rs.getTimestamp("created_date"));
            reciept.setTotal(rs.getFloat("total"));
            reciept.setCrash(rs.getFloat("crash"));
            reciept.setTotal(rs.getInt("total_qty"));
            reciept.setUserId(rs.getInt("user_id"));
            reciept.setCustomerId(rs.getInt("customer_id"));
            //Population

            CustomerDao customerDao = new CustomerDao();
            UserDao userDao = new UserDao();
            Customer customer = customerDao.get(reciept.getCustomerId());
            User user = userDao.get(reciept.getUserId());
            reciept.setCustomer(customer);
            reciept.setUser(user);
        } catch (SQLException ex) {
            Logger.getLogger(Reciept.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return reciept;
    }

}
