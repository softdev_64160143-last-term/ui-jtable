/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.UserDao;
import com.werapan.databaseproject.model.User;
import java.util.List;

/**
 *
 * @author werapan
 */
public class UserService {
    public static User currentUser;
    public User login(String login, String password) {
        UserDao userDao = new UserDao();
        User user = userDao.getByLogin(login);
        if(user != null && user.getPassword().equals(password)) {
            currentUser = user;
            return user;
        }
        return null;
    }

    public static User getCurrentUser() {
        return new User(6,"purin","purin duang","Password",0,"M");
    }
    
    public List<User> getUser(){
        UserDao userDao = new UserDao();
        return userDao.getAll("user_login asc");
    }

    public User addNew(User edtedUser)throws Exception{
        if(!edtedUser.isValid()){
            throw  new Exception("User is invalid");
        }
            UserDao userDao = new UserDao();
            return userDao.save(edtedUser);
    }

    public User update(User edtedUser) {
        UserDao userDao = new UserDao();
        return userDao.update(edtedUser);
    }
    
  

    public int delete(User edtedUser) {
        UserDao userDao = new UserDao();
        return userDao.delete(edtedUser);
    }
  
}
