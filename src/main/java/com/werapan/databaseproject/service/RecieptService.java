/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.RecieptDao;
import com.werapan.databaseproject.dao.RecieptDetailDao;
import com.werapan.databaseproject.model.Reciept;
import com.werapan.databaseproject.model.RecieptDetail;
import java.util.List;

/**
 *
 * @author werapan
 */
public class RecieptService {
    public Reciept getById(int id){
        RecieptDao RecieptDao = new RecieptDao();
        return RecieptDao.get(id);
    }
    
    public List<Reciept> getReciept(){
        RecieptDao RecieptDao = new RecieptDao();
        return RecieptDao.getAll("customer_id asc");
    }

    public Reciept addNew(Reciept edtedReciept) {
            RecieptDao recieptDao = new RecieptDao();
            RecieptDetailDao RecieptDetailDao = new RecieptDetailDao();
            Reciept reciept =  RecieptDao.save(edtedReciept);
            for(RecieptDetail rd: edtedReciept.getRecieptDetail()){
                rd.setRecieptId(reciept.getId());
                RecieptDetailDao.save(rd);
            }
    }

    public Reciept update(Reciept edtedReciept) {
        RecieptDao RecieptDao = new RecieptDao();
        return RecieptDao.update(edtedReciept);
    }
    
  

    public int delete(Reciept edtedReciept) {
        RecieptDao RecieptDao = new RecieptDao();
        return RecieptDao.delete(edtedReciept);
    }
  
}
